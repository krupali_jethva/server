package com.example.quotes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuotesMasterApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuotesMasterApplication.class, args);
	}

}
