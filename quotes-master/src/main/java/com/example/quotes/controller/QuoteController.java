package com.example.quotes.controller;

import java.time.Duration;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.quotes.model.Quote;
import com.example.quotes.repository.QuoteMongoReactiveRepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

//Reactive controller
@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class QuoteController {

	@Autowired
	private QuoteMongoReactiveRepository quoteMongoReactiveRepository;

	@PostMapping("/quote/add-quote")
	public Mono<Quote> createQuote(@Valid @RequestBody Quote quote) {
		return quoteMongoReactiveRepository.save(quote);
	}

	@GetMapping("/quote/get-all-quote")
	public Flux<Quote> getAllQuotes() {
		System.out.println("Get all Quote...");
		return quoteMongoReactiveRepository.findAll();// .delayElements(Duration.ofMillis(1000));
	}

	@DeleteMapping("/quote/{id}")
	public ResponseEntity<String> deleteQuote(@PathVariable("id") String id) {
		System.out.println("Delete Customer with ID = " + id + "...");

		try {
			quoteMongoReactiveRepository.deleteById(id).subscribe();
		} catch (Exception e) {
			return new ResponseEntity<>("Fail to delete!", HttpStatus.EXPECTATION_FAILED);
		}

		return new ResponseEntity<>("Quote has been deleted!", HttpStatus.OK);
	}

	@GetMapping("/quotedetail/{id}")
	public Mono<Quote> getQuote(@PathVariable("id") String id) {
		System.out.println("Fetch Quote with ID = " + id + "...");
		return quoteMongoReactiveRepository.findById(id);
	}

}
