package com.example.quotes.model;

import java.io.Serializable;

public class Quote implements Serializable {

	private String id;
	private String appId;
	private String quoteContent;
	private String siteUrl;
	private String metaKeywords;
	private String pageCount;
	private String quoteSelector;
	private String autherName;
	private String quoteMaxChars;
	private String updateBy;
	private String createBy;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public String getQuoteContent() {
		return quoteContent;
	}
	public void setQuoteContent(String quoteContent) {
		this.quoteContent = quoteContent;
	}
	public String getSiteUrl() {
		return siteUrl;
	}
	public void setSiteUrl(String siteUrl) {
		this.siteUrl = siteUrl;
	}
	public String getMetaKeywords() {
		return metaKeywords;
	}
	public void setMetaKeywords(String metaKeywords) {
		this.metaKeywords = metaKeywords;
	}
	public String getPageCount() {
		return pageCount;
	}
	public void setPageCount(String pageCount) {
		this.pageCount = pageCount;
	}
	public String getQuoteSelector() {
		return quoteSelector;
	}
	public void setQuoteSelector(String quoteSelector) {
		this.quoteSelector = quoteSelector;
	}
	public String getAutherName() {
		return autherName;
	}
	public void setAutherName(String autherName) {
		this.autherName = autherName;
	}
	public String getQuoteMaxChars() {
		return quoteMaxChars;
	}
	public void setQuoteMaxChars(String quoteMaxChars) {
		this.quoteMaxChars = quoteMaxChars;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	@Override
	public String toString() {
		return "Quote [id=" + id + ", appId=" + appId + ", quoteContent=" + quoteContent + ", siteUrl=" + siteUrl
				+ ", metaKeywords=" + metaKeywords + ", pageCount=" + pageCount + ", quoteSelector=" + quoteSelector
				+ ", autherName=" + autherName + ", quoteMaxChars=" + quoteMaxChars + ", updateBy=" + updateBy
				+ ", createBy=" + createBy + "]";
	}
	


}
