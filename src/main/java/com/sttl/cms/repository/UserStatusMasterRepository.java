package com.sttl.cms.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sttl.cms.model.UserStatusMaster;

public interface UserStatusMasterRepository extends JpaRepository<UserStatusMaster, Long>  {

}
