package com.sttl.cms.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sttl.cms.model.User;

@Repository 
@Transactional
public interface UserRepository extends JpaRepository<User, Long>{
	
	 @Modifying 
     @Query("UPDATE User u SET u.profileImage = :profileImage WHERE u.id = :userId")
     int updateProfileImage(@Param("userId") Long userId, @Param("profileImage") String profileImage);

	
	
}
