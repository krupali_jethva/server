package com.sttl.cms.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sttl.cms.model.RoleMaster;

public interface RoleMasterRepository extends JpaRepository<RoleMaster, Long> {

}
