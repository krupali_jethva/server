package com.sttl.cms.controller;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sttl.cms.model.CaptchaModel;
import com.sttl.cms.model.RoleMaster;
import com.sttl.cms.model.User;
import com.sttl.cms.model.UserStatusMaster;
import com.sttl.cms.repository.RoleMasterRepository;
import com.sttl.cms.repository.UserStatusMasterRepository;
import com.sttl.cms.service.UserService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/admin")
public class UserController {

	@Autowired
	UserService userservice;

	@Autowired
	RoleMasterRepository roleMasterRepository;

	@Autowired
	UserStatusMasterRepository userStatusMasterRepository;

	@PostMapping(value = "/saveUser")
	public ResponseEntity<User> saveUser(@RequestBody User user) {
		System.out.println("In saveUser"+user.toString());
		//try {
			User _user = userservice.saveUser(user);
			return new ResponseEntity<>(_user, HttpStatus.CREATED);
		//} catch (Exception e) {
			//return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED); // TODO: handle exception
		//}
	}

	@PostMapping(value = "/saveRole")
	public ResponseEntity<RoleMaster> saveRole(@RequestBody RoleMaster roleMaster) {
		try {
			System.out.println("In saveRole" + roleMaster.toString());
			RoleMaster _roleMaster = roleMasterRepository.save(roleMaster);
			return new ResponseEntity<>(_roleMaster, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED); // TODO: handle exception
		}
	}

	@GetMapping(value = "/users")
	public ResponseEntity<List<User>> getAllUser() {
		List<User> userList = new ArrayList<>();
		System.out.println("In users");
		try {
			userservice.findAllUser().forEach(userList::add);
			if (userList.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(userList, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR); // TODO: handle exception
		}
	}

	@GetMapping(value = "/users/{userId}")
	public User findOne(@PathVariable("userId") Long id) {

		return userservice.getOneUser(id);
	}

	@GetMapping(value = "/roles")
	public ResponseEntity<List<RoleMaster>> getAllRole() {

		List<RoleMaster> roleList = new ArrayList<>();
		System.out.println("In users");
		try {
			roleMasterRepository.findAll().forEach(roleList::add);
			if (roleList.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(roleList, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR); // TODO: handle exception
		}
	}

	@GetMapping(value = "/roleEdit/{id}")
	public ResponseEntity<RoleMaster> roleEdit(@PathVariable(value = "id") Long id) {
		Optional<RoleMaster> roleList = roleMasterRepository.findById(id);
		System.out.println("In Edit Role" + roleList.get().toString());
		if (roleList.isPresent()) {
			return new ResponseEntity<>(roleList.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PutMapping(path = { "updateRole/{id}" })
	public ResponseEntity<RoleMaster> update(@PathVariable("id") Long id, @RequestBody RoleMaster role) {
		role.setId(id);
		try {
			RoleMaster _role = roleMasterRepository.save(role);
			return new ResponseEntity<>(_role, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED); // TODO: handle exception
		}
	}

	@DeleteMapping(path = "roleDelete/{id}")
	public ResponseEntity<HttpStatus> delete(@PathVariable("id") Long id) {
		try {
			roleMasterRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED); // TODO: handle exception
		}
	}

	@GetMapping(value = "/status")
	public ResponseEntity<List<UserStatusMaster>> getAllStatus() {

		List<UserStatusMaster> statusList = new ArrayList<>();
		System.out.println("In users");
		try {
			userStatusMasterRepository.findAll().forEach(statusList::add);
			if (statusList.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(statusList, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR); // TODO: handle exception
		}
	}
	
	@GetMapping("/getCaptchaImagefdhfh")
	public CaptchaModel captchaImage(HttpServletRequest request, HttpServletResponse response) 
	{
		try {
			HttpSession session = request.getSession(true);
			
			Random rdm1=new Random();
			Random rdmop=new Random();
			int num1=rdm1.nextInt(10);
			int num2=rdm1.nextInt(10);
			int op1=rdmop.nextInt(2);
			int res=0;
			String captchaCode;
			while(num1==0)
			{
				num1=rdm1.nextInt(10);	
				
			}
			while(num2==0)
			{
				num2=rdm1.nextInt(10);
				
			}
			captchaCode=Integer.toString(num1);
			if(op1==0 || op1==1)
			{
				captchaCode=captchaCode+" + ";
			}
			
			captchaCode=captchaCode+Integer.toString(num2)+" = ";
			if(op1==0 || op1==1)
			{
				res=num1+num2;
			}			
			
			String rstr=Integer.toString(res);
			System.out.println("rstr : "+rstr);
			session.setAttribute("captchaCode", rstr);
			
			
			int width = 150;
			int height = 40;
			
			Color background = new Color(255,255,255);
			Color color = new Color(0, 0, 0);
			
			Font font = new Font("Times New Roman", Font.PLAIN, 20);
			
			BufferedImage cpImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
			
			Graphics g = cpImage.createGraphics();
			g.setColor(background);
			g.fillRect(0, 0, width, height);
			g.setColor(color);
			g.setFont(font);
			g.drawString(captchaCode, 20, 20);
			g.setColor(background);
			g.drawLine(25, 20, 120, 20);
			
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			ImageIO.write(cpImage, "jpeg", stream);
			stream.close();
			String str=Base64.getEncoder().encodeToString(stream.toByteArray());
			CaptchaModel captchaModel=new CaptchaModel();
			captchaModel.setCaptchaImage(str);
			captchaModel.setCaptchaCode(res);
			return captchaModel;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new CaptchaModel();
	}
	
	@GetMapping(value ="/getCaptchaImage")
	public CaptchaModel captchaImage(HttpServletResponse response) 
	{
		try {
			
			/* ********** Create number captcha Start ********** */
			//OutputStream outputStream = response.getOutputStream();
		//	PrintWriter out = new PrintWriter(outputStream);
		
			Random rdm1=new Random();			
			Random rdmop=new Random();
			int num1=rdm1.nextInt(10);
			int num2=rdm1.nextInt(10);
			int op1=rdmop.nextInt(2);
			int res=0;
			String captchaCode;
			while(num1==0)
			{
				num1=rdm1.nextInt(10);	
				
			}
			while(num2==0)
			{
				num2=rdm1.nextInt(10);
				
			}		
			//out.print(num1);
		captchaCode=Integer.toString(num1);
			if(op1==0 || op1==1)
			{
				captchaCode=captchaCode+" + ";
			}
			
			captchaCode=captchaCode+Integer.toString(num2)+" = ";		
			if(op1==0 || op1==1)
			{
				res=num1+num2;
			}			
			
			String rstr=Integer.toString(res);
			System.out.println("rstr : "+rstr);
			/* ********** Create number captcha Start ********** */
			
			
			/* ********** Create Captcha Image Start ********** */
			int width = 150;
			int height = 40;
			
			Color background = new Color(255,255,255);
			Color color = new Color(0, 0, 0);
			
			Font font = new Font("Times New Roman", Font.PLAIN, 20);
			
			BufferedImage cpImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
			
			Graphics g = cpImage.createGraphics();
			g.setColor(background);
			g.fillRect(0, 0, width, height);
			g.setColor(color);
			g.setFont(font);
			g.drawString(captchaCode, 20, 20);
			g.setColor(background);
			g.drawLine(25, 20, 120, 20);

			response.setContentType("image/jpeg");	
			
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();			
	        ImageIO.write(cpImage, "png", baos);
	        
	        //convert image to encode	        
	        String data=Base64.getEncoder().encodeToString(baos.toByteArray());
	      //  String imageString = "data:image/png;base64," + data;	        
		    System.out.println("imageString : "+data);
	        baos.close();
		//	out.flush();
			//out.close();
			/* ********** Create Captcha Image End ********** */
			CaptchaModel captchaModel=new CaptchaModel();

			captchaModel.setCaptchaCode(res);
			captchaModel.setCaptchaImage(data);	
			return captchaModel;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return new CaptchaModel();
		
	}

	
//	@GetMapping(value="/users/{userId}")
//	public ResponseEntity<User> getUser(@PathVariable Long userId) {	
//		User user= userservice.getOneUser(userId);
//		if(user==null)
//		{
//			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//		}	
//		return new ResponseEntity<>(user,HttpStatus.OK);
//	}
//	

//	@PostMapping("/uploadImage/{userId}")
//	@ResponseStatus(HttpStatus.OK)
//	public void handleFileUpload(@PathVariable int userId, @RequestParam("file") MultipartFile file)
//			throws IOException {
//		System.out.println("uploadImage :: "+userId);
//		//fileService.storeFile(file);
//	}
}
