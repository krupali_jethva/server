package com.sttl.cms.controller;

import java.io.IOException;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.sttl.cms.service.FileService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/attachement")
public class FileController {

	private final FileService fileService;
	 
	@Autowired
	public FileController(FileService fileService) {
		this.fileService = fileService;
	}
 
	@PostMapping(value = "/files")
	@ResponseStatus(HttpStatus.OK)
	public void handleFileUpload(@RequestParam("file") MultipartFile file) throws IOException {
		System.out.println("in file upload...");
		fileService.storeFile(file);
	}
	
	@PostMapping(value = "/uploadImage/{userId}")
	public int handleFileUpload(@PathVariable Long userId, @RequestParam("file") MultipartFile file,
			HttpSession session) throws IOException {
		System.out.println("in controller");
		return fileService.store(file, userId, session);
	}
	
}
