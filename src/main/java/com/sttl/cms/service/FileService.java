package com.sttl.cms.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.sttl.cms.model.User;
import com.sttl.cms.repository.UserRepository;

@Component
public class FileService {

	final static String FILE_DIRECTORY = "file.repository.path";
	final static String fileSeparator = File.separator;

	@Autowired
	private Environment environment;
	
	@Autowired
	UserRepository repository;

	public void storeFiledgdf(MultipartFile file) throws IOException {
		Path filePath = Paths.get(environment.getProperty(FILE_DIRECTORY) + fileSeparator + file.getOriginalFilename());

		Files.copy(file.getInputStream(), filePath, StandardCopyOption.REPLACE_EXISTING);
	}

	public boolean storeFile(MultipartFile file) throws IOException {


		try {
			String fileName = generateDocumentName() + file.getOriginalFilename();
			System.out
					.println("saveAttachment : " + environment.getProperty(FILE_DIRECTORY) + fileSeparator + fileName);

//			File oldFile = new File(environment.getProperty(FILE_DIRECTORY) + fileSeparator + fileName);
//			
//			System.out.println("oldFile.exists() :: " + oldFile.exists());
			
			File destination = new File(environment.getProperty(FILE_DIRECTORY).concat(fileName));

			Files.createDirectories(Paths.get(destination.getAbsolutePath()).getParent());
			Files.copy(file.getInputStream(),
					Paths.get(environment.getProperty(FILE_DIRECTORY) + fileSeparator + fileName),
					StandardCopyOption.REPLACE_EXISTING);

			file.getInputStream().close();
			return true;
		} catch (FileAlreadyExistsException fe) {
			System.err.println("Exception : File Already Exists so printStackTrace not printed.");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	public String generateDocumentName() {
		return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyMMddhhmmssMsn"));
	}

	public int store(MultipartFile file, Long userID, HttpSession session) throws IOException {
	    Path rootLocation = Paths.get(session.getServletContext().getRealPath(environment.getProperty(FILE_DIRECTORY)));  
        
        System.out.println("rootLocation  ==  " + rootLocation);  
          
         User userDetail = repository.getOne(userID);
           
         System.out.println("userDetail"+userDetail.toString());
         
         String nameExtension[] = file.getContentType().split("/");  
  
         String profileImage = userID + generateDocumentName() +"." + nameExtension[1];  
              
         System.out.println("ProfileImage  :: " + profileImage);  
           
         if(userDetail.getId() > 0 )  
         {  
               
            if(userDetail.getProfileImage() == null || userDetail.getProfileImage() == " " || userDetail.getProfileImage() == "" )  
            {  
//                try  
//                {  
                	File destination = new File(environment.getProperty(FILE_DIRECTORY).concat(profileImage));

        			Files.createDirectories(Paths.get(destination.getAbsolutePath()).getParent());
        			Files.copy(file.getInputStream(),
        					Paths.get(environment.getProperty(FILE_DIRECTORY) + fileSeparator + profileImage),
        					StandardCopyOption.REPLACE_EXISTING);

        			file.getInputStream().close();
                   // Files.copy(file.getInputStream(),rootLocation.resolve(profileImage));  
                    int result = repository.updateProfileImage(userID, profileImage);    
                    if(result > 0)  
                        return result;  
                    else  
                        return -5;  
//                }  
//                catch(Exception exception)  
//                {  
//                    System.out.println("error while uploading image catch:: " + exception.getMessage());  
//                    return -5;  
//                }  
            }  
            else  
            {  
                try  
                {  
                    Files.delete(rootLocation.resolve(profileImage));  
                      
                    Files.delete(rootLocation.resolve(userDetail.getProfileImage()));  
                      
                    Files.copy(file.getInputStream(),rootLocation.resolve(profileImage));  
                    int result = repository.updateProfileImage(userID, profileImage);    
                    if(result > 0)  
                        return result;  
                    else  
                        return -5;  
                }  
                catch(Exception exception)  
                {  
                    System.out.println("Error while uploading image when image is already Exists :: " + exception.getMessage());  
                    return -5;  
                }  
            }  
        }  
        else {  
            return 0;  
        }  
    }  

}
