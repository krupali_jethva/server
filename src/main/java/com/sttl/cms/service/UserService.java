package com.sttl.cms.service;

import com.sttl.cms.model.User;

public interface UserService {

	User saveUser(User user);

	Iterable<User> findAllUser();

	User getOneUser(Long userId);

	
	
}
