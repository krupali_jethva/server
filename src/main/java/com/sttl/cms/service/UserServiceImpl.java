package com.sttl.cms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sttl.cms.model.User;
import com.sttl.cms.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService  {

	@Autowired
	UserRepository repository;

	@Override
	public User saveUser(User user) {
		// TODO Auto-generated method stub
		return repository.save(user);
	}

	@Override
	public Iterable<User> findAllUser() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

	@Override
	public User getOneUser(Long userId) {
		// TODO Auto-generated method stub
		return repository.getOne(userId);
	}

}
