package com.sttl.cms.model;

public class CaptchaModel {

	private String captchaImage;

	private Integer captchaCode;

	public String getCaptchaImage() {
		return captchaImage;
	}

	public void setCaptchaImage(String captchaImage) {
		this.captchaImage = captchaImage;
	}

	public Integer getCaptchaCode() {
		return captchaCode;
	}

	public void setCaptchaCode(Integer captchaCode) {
		this.captchaCode = captchaCode;
	}

}
