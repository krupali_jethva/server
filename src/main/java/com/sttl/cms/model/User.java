package com.sttl.cms.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(schema="public",name = "user_master")
@JsonIgnoreProperties(ignoreUnknown = true)
public class User extends DefaultColumns implements Serializable  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id 
	@SequenceGenerator(name="user_master_id_seq", sequenceName="user_master_id_seq",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="user_master_id_seq")
	@Column(name="id", unique=true, nullable=false)
	private Long id;

	@Column(name="first_name")
	private String firstName;
	
	@Column(name="last_name")
	private String lastName;
	
	@Column(name="user_name")
	private String username;	
	
	@Column(name="email")
    private String email;
    
	@Column(name="user_pwd")
    private String password;
	
	@Column(name="profile_image")  
    public String profileImage;  
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="status_id")
	private UserStatusMaster statusMaster;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="role_id")
	private RoleMaster roleMaster;

	@Column(name="is_super_admin")
	private Boolean isSuperAdmin=false;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}

	public UserStatusMaster getStatusMaster() {
		return statusMaster;
	}

	public void setStatusMaster(UserStatusMaster statusMaster) {
		this.statusMaster = statusMaster;
	}

	public RoleMaster getRoleMaster() {
		return roleMaster;
	}

	public void setRoleMaster(RoleMaster roleMaster) {
		this.roleMaster = roleMaster;
	}

	public Boolean getIsSuperAdmin() {
		return isSuperAdmin;
	}

	public void setIsSuperAdmin(Boolean isSuperAdmin) {
		this.isSuperAdmin = isSuperAdmin;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", username=" + username
				+ ", email=" + email + ", password=" + password + ", profileImage=" + profileImage + ", statusMaster="
				+ statusMaster + ", roleMaster=" + roleMaster + ", isSuperAdmin=" + isSuperAdmin + "]";
	}
	
}
