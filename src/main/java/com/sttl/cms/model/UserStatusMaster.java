package com.sttl.cms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(schema="public", name="user_status_master")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class UserStatusMaster {
	@Id
	@SequenceGenerator(name="status_master_id_seq",sequenceName="status_master_id_seq",schema = "public",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="status_master_id_seq")
	@Column(name="id",unique=true,nullable=false)
	private Long id;
	
	@Column(name="status",length=25,nullable=false)
	private String status;
	
	@Column(name="is_active",nullable=false)
	private Boolean isActive = true;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	@Override
	public String toString() {
		return "UserStatusMaster [id=" + id + ", status=" + status + ", isActive=" + isActive + "]";
	}

		
}
