package com.sttl.cms.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(schema="public",name="site_master")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class SiteMaster extends DefaultColumns implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id 
	@SequenceGenerator(name="site_master_id_seq", sequenceName="site_master_id_seq",schema = "public", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="site_master_id_seq")
	@Column(name="id", unique=true, nullable=false)
	private Integer id;
	
	@Column(name="name", nullable=false, length=100)
	private String name;

	@Column(name="site_url", length=300)
	private String siteUrl;
	
	@Column(name="application_path", length=100)
	private String applicationPath;
	
	@Column(name="physical_path", length=100)
	private String physicalPath;
	
	@Column(name="hit_count")
	private Long hitCount;

	public SiteMaster() {}
	public SiteMaster(Integer id) {
		super();
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSiteUrl() {
		return siteUrl;
	}

	public void setSiteUrl(String siteUrl) {
		this.siteUrl = siteUrl;
	}

	public String getApplicationPath() {
		return applicationPath;
	}

	public void setApplicationPath(String applicationPath) {
		this.applicationPath = applicationPath;
	}

	public String getPhysicalPath() {
		return physicalPath;
	}

	public void setPhysicalPath(String physicalPath) {
		this.physicalPath = physicalPath;
	}

	public Long getHitCount() {
		return hitCount;
	}

	public void setHitCount(Long hitCount) {
		this.hitCount = hitCount;
	}
	@Override
	public String toString() {
		return "SiteMaster [id=" + id + ", name=" + name + ", siteUrl=" + siteUrl + ", applicationPath="
				+ applicationPath + ", physicalPath=" + physicalPath + ", hitCount=" + hitCount + ", getId()=" + getId()
				+ ", getName()=" + getName() + ", getSiteUrl()=" + getSiteUrl() + ", getApplicationPath()="
				+ getApplicationPath() + ", getPhysicalPath()=" + getPhysicalPath() + ", getHitCount()=" + getHitCount()
				+ ", getCreatedDate()=" + getCreatedDate() + ", getCreatedBy()=" + getCreatedBy() + ", getIpAddress()="
				+ getIpAddress() + ", getUpdatedDate()=" + getUpdatedDate() + ", getUpdatedBy()=" + getUpdatedBy()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
				+ "]";
	}
	
	
}
