package com.sttl.cms.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(schema="public", name="role_master")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class RoleMaster extends DefaultColumns implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id 
	@SequenceGenerator(name="role_master_id_seq", sequenceName="role_master_id_seq", schema = "public", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="role_master_id_seq")
	@Column(name="id", unique=true, nullable=false)
	private Long id;
	
	@Column(name="role_name", length=50)
	private String roleName;
	
	@Column(name="role_desc", length=100)
	private String roleDescription;
	
	@Column(name="role_order", nullable=false)
	private Integer roleOrder;
	
	@Column(name="is_active",nullable=false)
	private boolean isActive=true;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="site_id")
	private SiteMaster siteId = new SiteMaster(1);
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleDescription() {
		return roleDescription;
	}

	public void setRoleDescription(String roleDescription) {
		this.roleDescription = roleDescription;
	}

	public Integer getRoleOrder() {
		return roleOrder;
	}

	public void setRoleOrder(Integer roleOrder) {
		this.roleOrder = roleOrder;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}


	public SiteMaster getSiteId() {
		return siteId;
	}

	public void setSiteId(SiteMaster siteId) {
		this.siteId = siteId;
	}

	@Override
	public String toString() {
		return "RoleMaster [id=" + id + ", roleName=" + roleName + ", roleDescription=" + roleDescription
				+ ", roleOrder=" + roleOrder + ", isActive=" + isActive + ", siteId=" + siteId + "]";
	}

	

}
